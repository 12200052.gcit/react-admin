import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import List from "./pages/list/List";
import Feedback from "./pages/feedback/Feedback";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/">
            <Route index element={<Login />} />
            <Route path="home" element={<Home />} />
            <Route path="users">
              <Route index element={<List />} />
            </Route>
            <Route path="feedbacks" element={<Feedback />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
