import "./sidebar.scss";
import DashboardIcon from "@mui/icons-material/Dashboard";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import StoreIcon from "@mui/icons-material/Store";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import { Link, useNavigate } from "react-router-dom";
import { logout } from "../../firebase";
const Sidebar = () => {
  let navigate = useNavigate();
  async function handleLogOut() {
    logout();
    navigate("/");
  }
  return (
    <div className="sidebar">
      <div className="top">
        <Link to="/home" style={{ textDecoration: "none" }}>
          <span className="logo">TradeHook</span>
        </Link>
      </div>
      <hr />
      <div className="bottom">
        <ul>
          <Link to="/users" style={{ textDecoration: "none" }}>
            <li>
              <PersonOutlineIcon className="icon" />
              <span>Users</span>
            </li>
          </Link>
        </ul>
        <ul>
          <Link to="/feedbacks" style={{ textDecoration: "none" }}>
            <li>
              <StoreIcon className="icon" />
              <span>Feedback</span>
            </li>
          </Link>
        </ul>
        <ul>
          <Link to="/" style={{ textDecoration: "none" }}>
            <li>
              <ExitToAppIcon className="icon" />
              <span>
                <button onClick={handleLogOut}>Logout</button>
              </span>
            </li>
          </Link>
        </ul>
      </div>
    </div>
  );
};

export default Sidebar;
