export const userColumns = [
  { field: "id", headerName: "ID", width: 100 },
  {
    field: "user",
    headerName: "User",
    width: 200,
    renderCell: (params) => {
      return (
        <div className="cellWithImg">
          <img src={params.row.img} alt="" className="cellImg" />
          {params.row.username}
        </div>
      );
    },
  },
  { field: "email", headerName: "Email", width: 200 },
  { field: "phone", headerName: "Phone Number", width: 200 },
];

export const productColumns = [
  { field: "iiid", header: "ID", width: 100 },
  {
    field: "owner",
    header: "Owner",
    width: 200,
    renderCell: (params) => {
      return (
        <div class="cellWithImg">
          <img src={params.row.downloadURL} alt="" class="cellImg" />
          {params.row.user}
        </div>
      );
    },
  },
  { field: "category", header: "Category", width: 200 },
  { field: "name", header: "Product Name", width: 200 },
  { field: "des", header: "Details", width: 200 },
  { field: "price", header: "Price", width: 200 },
  { field: "phone", header: "Phone Number", width: 200 },
];
