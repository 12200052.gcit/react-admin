import { initializeApp } from "firebase/app";
import { getAuth, signOut } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyBxFTgr8zVi5zEDYK_Z_woew2ycX5HuV1c",
  authDomain: "tradehook-464e9.firebaseapp.com",
  projectId: "tradehook-464e9",
  storageBucket: "tradehook-464e9.appspot.com",
  messagingSenderId: "551210395357",
  appId: "1:551210395357:web:9a7f6733e8754d8cce9d34",
};

const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const auth = getAuth();
export const storage = getStorage(app);

export function logout() {
  signOut(auth);
}