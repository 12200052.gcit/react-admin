import { collection, query, getDocs, deleteDoc, doc } from "firebase/firestore";
import React, { useEffect, useState } from "react";
import { db } from "../../firebase";
import "./feedback.css";
function Feedback() {
  const [feedbacks, setFeedbacks] = useState([]);

  const userData = async () => {
    const q = query(collection(db, "Feedbacks"));
    const querySnapshot = await getDocs(q);
    const data = querySnapshot.docs.map((doc) => ({
      // doc.data() is never undefined for query doc snapshots
      ...doc.data(),
      id: doc.id,
    }));
    setFeedbacks(data);
  };
  useEffect(() => {
    userData();
  }, []);

  const handleDelete = async (id) => {
    if (window.confirm("Are you sure you want to delete this feedback?")) {
      try {
        await deleteDoc(doc(db, "Feedbacks", id));
        setFeedbacks(feedbacks.filter((item) => item.id !== id));
      } catch (error) {
        console.log(error);
      }
    }
  };
  return (
    <div className="root">
      <div className="container">
        <div className="headerC">
          <p className="header">Feedbacks</p>
        </div>
        <div className="feedback">
          <table>
            <thead>
              <tr>
                <th>#</th>
                <th>feedback</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {feedbacks.map((doc, index) => {
                return (
                  <tr key={doc.id}>
                    <td>{index + 1}</td>
                    <td>{doc.userid}</td>
                    <td>{doc.feedback}</td>
                    <td>
                      <div>
                        <button
                          className="delete"
                          onClick={() => handleDelete(doc.id)}
                        >
                          Delete
                        </button>
                      </div>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default Feedback;
